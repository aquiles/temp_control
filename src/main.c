#include <signal.h>
#include <pthread.h>
#include "external_sensor.h"
#include "uart_control.h"
#include "input.h"
#include "lcd.h"

/******** global var ********/
int uart0_filestream;
int8_t i2c_fd;

/********** function **************/

void display_lcd(float temp_ext,float temp_int)
{
	lcdLoc(LINE1);
	typeln("TI:");
	typeFloat(temp_int);
	typeln(" TE:");
	typeFloat(temp_ext);
	lcdLoc(LINE2);
	typeln("TR:");
	typeFloat(temp_user);
	//ClrLcd();
}

void clean(int sig)
{
	printf("fechando ...\n");
	digitalWrite(18, LOW);
	digitalWrite(16, LOW);
	close(uart0_filestream);
	close(i2c_fd);
	exit(0);
}
void do_nothing(int sig)
{
	return;
}
/*********** main ***********/
int main(){	
	signal(SIGINT, clean);
	signal(SIGALRM,do_nothing);
	
	// init devices
	uart0_filestream = configure_uart();
	if (i2c_fd = open("/dev/i2c-1", O_RDWR) == -1) return -1;
	lcd_init(); // setup LCD
	
	// resistor
	pinMode(16, OUTPUT) ;

	// fan
	pinMode(18, OUTPUT) ;
	
	float temp_ext, temp_int,temp_ref_bottom,temp_ref_ceil;
	int timer = 4, choice = 0,first = 1;
	char  char_time[20];
	struct tm *time_local;
	time_t timeref;

	choice = choice_mode();
	choice_temp(choice);
	ualarm(500000,500000);
	while (1)
	{
		if(choice == 1 && !first)
		{
			temp_user = read_uart(uart0_filestream);
		}

		//temp_ext = main_bme(i2c_fd);
		request(0xa1,uart0_filestream);
		pause();
		//usleep(500);

		if(choice == 1)
		{
			request(0xa2,uart0_filestream);
			first = 0;
		}
		temp_int = read_uart(uart0_filestream);
		
		//lcd display
		if(!first && choice == 1 || choice == 2)
		{
			display_lcd(temp_ext,temp_int);
			temp_ref_bottom = temp_user - (hist/2);
			temp_ref_ceil = temp_user + (hist/2);
			if(temp_int > temp_ref_ceil)
			{
				digitalWrite(18, HIGH);
				digitalWrite(16, LOW);
			}
			else if(temp_int < temp_ref_bottom)
			{
				digitalWrite(18, LOW);
				digitalWrite(16, HIGH);
			}
			else
			{
				digitalWrite(18, LOW);
				digitalWrite(16, LOW);
			}
		}
		timer--;
		if(!timer){
			// reset counter
			timer = 4;

			//take date and time from now.
			time(&timeref);
			time_local = localtime(&timeref);
			strftime(char_time,20,"%x, %X",time_local);

			FILE *fp = fopen("log.csv","a");
			// Data e hora, temperatura interna, temperatura externa, temperatura definida pelo usuário
			fprintf(fp,"%s, %0.2lf deg C, %0.2lf deg C, %0.2lf deg C\n", char_time,temp_int,temp_ext,temp_user);
			printf("%s, %0.2lf deg C, %0.2lf deg C, %0.2lf deg C\n", char_time,temp_int,temp_ext,temp_user);

    		fclose(fp);
		}
	}
}
