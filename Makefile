comp:
	gcc -O3 ./src/main.c ./inc/drivers/bme280.c -I ./inc -I ./inc/drivers -lwiringPi -o ./bin/bin
debug:
	gcc -g ./src/main.c ./inc/drivers/bme280.c -I ./inc -I ./inc/drivers -lwiringPi -o ./bin/bin
run:
	./bin/bin
clean:
	rm -v ./bin/bin
	rm -v log.csv
	echo 'Data, hora, temperatura interna, temperatura externa, temperatura definida pelo usuário' > log.csv
