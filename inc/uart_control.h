#include <stdio.h>
#include <unistd.h>         //Used for UART
#include <fcntl.h>          //Used for UART
#include <termios.h>        //Used for UART
#include <string.h>
#include <stdlib.h>

int configure_uart(){
	int uart0_filestream = -1;
	uart0_filestream = open("/dev/serial0", O_RDWR | O_NOCTTY | O_NDELAY);      //Open in non blocking read/write mode
    if (uart0_filestream == -1)
    {
        printf("Erro - Não foi possível iniciar a UART.\n");
        close(uart0_filestream);
		exit(-1);
    }
    else
    {
        printf("UART inicializada!\n");
    }
    struct termios options;
    tcgetattr(uart0_filestream, &options);
    options.c_cflag = B115200 | CS8 | CLOCAL | CREAD;     //<Set baud rate
    options.c_iflag = IGNPAR;
    options.c_oflag = 0;
    options.c_lflag = 0;
    tcflush(uart0_filestream, TCIFLUSH);
    tcsetattr(uart0_filestream, TCSANOW, &options);
	return uart0_filestream;
}

float read_uart(int uart0_filestream){
	// Read up to 255 characters from the port if they are there
    float rx_buffer;

    int rx_length = read(uart0_filestream, (void *)&rx_buffer, sizeof(float));      //Filestream, buffer to store in, number of bytes to read (max)
    if (rx_length < 0)
    {
        printf("Erro na leitura.\n"); //An error occured (will occur if there are no bytes)
        return -1;
    }
    else if (rx_length == 0)
    {
        printf("Nenhum dado disponível.\n"); //No data waiting
		return -1;
    }
    else
    {
		return rx_buffer;
    }
}

void write_uart(int uart0_filestream,char data[],int size){
//	printf("Escrevendo caracteres na UART ...");
	int count = write(uart0_filestream, data, size);
	if (count < 0)
	{
		printf("UART TX error\n");
	}
	else
	{
//		printf("escrito.\n");
		return;
	}
}

void request(int cmd,int uart0_filestream){
	char data[] = {cmd,0,3,3,1};
	write_uart(uart0_filestream,data,5);
}

float main_uart(int uart0_filestream,int cmd) {
	float result;
	if(cmd > 0XA0  && cmd < 0XA3)
		request(cmd,uart0_filestream);
	else{
		printf("comando invalido !!! \n");
		close(uart0_filestream);
		exit(-1);
	}
	return read_uart(uart0_filestream);
}
